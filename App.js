import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import NavBtn from './src/components/buttons/NavBtn';
import Mode1Screen from './src/screens/Mode1Screen';
import Mode2Screen from './src/screens/Mode2Screen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Mode1"
          component={Mode1Screen}
          options={({ navigation, route }) => ({
            headerRight: () => (
              <NavBtn
                onPress={() => navigation.navigate('Mode2')}
              >Включить режим 2</NavBtn>
            )
          })}
        />
        <Stack.Screen
          name="Mode2"
          component={Mode2Screen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}