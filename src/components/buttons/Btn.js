import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const Btn = props => (
  <TouchableOpacity style={[st.wrap, props.style]} onPress={props.onPress}>
    <Text style={st.text}>{props.children}</Text>
  </TouchableOpacity>
);

Btn.defaultProps = {
  children: 'Press me',
  onPress: () => { }
}

const st = StyleSheet.create({
  wrap: {
    height: 40,
    width: 270,
    borderRadius: 8,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: 'white',
    fontSize: 14
  }
})

export default Btn;