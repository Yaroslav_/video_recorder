import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const NavBtn = props => (
  <TouchableOpacity style={[st.wrap, props.style]} onPress={props.onPress}>
    <Text style={st.text}>{props.children}</Text>
  </TouchableOpacity>
);

NavBtn.defaultProps = {
  children: 'Press me',
  onPress: () => { }
}

const st = StyleSheet.create({
  wrap: {
    height: 40,
    borderRadius: 8,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: '#000',
    fontSize: 14
  }
})

export default NavBtn;