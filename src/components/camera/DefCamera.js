import React, { useRef, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { RNCamera } from 'react-native-camera';

const DefCamera = props => {
  const cameraRef = useRef(null);

  const onCameraReady = async () => {
    props.onCameraReady && props.onCameraReady();
  };

  useEffect(() => {
    const stopAndSave = async () => {
      try {
        await cameraRef.current.stopRecording();

      } catch (err) { console.log(err) }
    }
    return () => { stopAndSave(); }
  }, [])

  useEffect(() => {
    if (props.startRecording) {
      try {
        if (cameraRef.current) {
          const options = {
            quality: RNCamera.Constants.VideoQuality["720p"],
          };
          if (props.duration) options.maxDuration = props.duration;
          if (!props.duration || props.duration && props.duration > 15) {
            options.maxFileSize = 12 * 1024 * 1024
          }

          cameraRef.current.recordAsync(options)
            .then(data => {
              const uri = data && data.uri ? data.uri : false
              props.onStop && props.onStop(uri);
            })
        }
      } catch (err) {
        props.onStop && props.onStop(false);
        console.log(err)
      }
    }
  }, [props.startRecording])

  return (
    <View style={[props.style]}>
      <View style={styles.container}>
        <RNCamera
          onCameraReady={onCameraReady}
          onRecordingStart={props.onRecordingStart}
          ref={cameraRef}
          style={styles.preview}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.on}
          ratio={'4:3'}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
      </View>
    </View>
  );

}

DefCamera.defaultProps = {
  onStop: () => { },
  onRecordingStart: () => { },
  style: {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    overflow: 'hidden'
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
});

export default DefCamera
