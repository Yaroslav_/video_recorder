import React, { useRef, useEffect, useState } from 'react';
import { StyleSheet, View, Dimensions, } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Video from 'react-native-video';
import Btn from '../buttons/Btn';
import ImagePicker from 'react-native-image-picker';
import CountDown from '../other/CountDown';

import MediaMeta from "rn-media-meta";
import useAudioDeviceConnected from '../../hooks/useAudioDeviceConnected';

const { width, height } = Dimensions.get('window');
const VIDEO_HEIGHT = height * 0.55;
const VIDEO_WIDHT = width * 0.4;
let prevVideoPick;

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  mediaType: 'video',
  fixOrientation: true,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const TwoVideos = props => {
  const cameraRef = useRef(null);
  const videoRef = useRef(null);
  const [videoData, setVideoData] = useState({ duration: false });
  const [isCountDown, setIsCountDown] = useState(false);
  const [isRecording, setIsRecording] = useState(false);
  const { isConnected } = useAudioDeviceConnected();
  const count = useRef(1);
  const duration = videoData.duration ? videoData.duration : videoData.duration === 0 ? 1 : false;

  const pickVideo = () => {
    ImagePicker.launchImageLibrary(options, async (response) => {
      if (response.didCancel) {
        return console.log('User cancelled image picker');
      } else if (response.error) {
        return console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        return console.log('User tapped custom button: ', response.customButton);
      } else {
        try {
          const data = await MediaMeta.get(response.path);
          count.current = 1;
          setVideoData({ ...response, ...data });
        } catch (err) {
          console.log('read file err', err)
        }
      }
    });
  }

  const toggleVideo = (val) => {
    videoRef.current.setNativeProps({
      paused: val
    })
  }

  const startRecording = () => {
    const options = {
      quality: RNCamera.Constants.VideoQuality["720p"],
    };
    if (duration) options.maxDuration = duration;
    if (!duration || duration && duration > 15) {
      options.maxFileSize = 12 * 1024 * 1024
    }

    cameraRef.current.recordAsync(options)
      .then(data => {
        const uri = data && data.uri ? data.uri : false;
        toggleVideo(true);
        count.current = 1;
        props.onStop && props.onStop(uri);
      })
  }

  const onStartRecordingPress = () => {
    if (isConnected) {
      setIsCountDown(true)
    } else {
      alert('Подключите наушники')
    }
  }

  const onRecordingStart = () => {
    toggleVideo(false);
  }

  const onEndTime = () => {
    setIsCountDown(false);
    startRecording();
    setIsRecording(true);
  }

  const toggleVideoOneTime = (val) => {
    if (count.current === 1) {
      count.current = 2;
      toggleVideo(val)
    }
  }

  const onStopAndSave = () => {
    cameraRef.current.stopRecording()
  }

  const onVideoLoad = data => setVideoData(state => ({ ...state, ...data }));

  const onVideoProgress = () => toggleVideoOneTime(true);

  useEffect(() => {
    if (prevVideoPick) {
      setVideoData(prevVideoPick);
    }
  }, [])

  useEffect(() => {
    count.current = 1;
    if (videoRef.current) {
      toggleVideo(false)
    }
    prevVideoPick = videoData;
  }, [videoData])


  return (
    <View style={st.container}>
      <View style={{ flexDirection: 'row', width: '100%', justifyContent: "space-evenly", }}>
        <RNCamera
          ref={cameraRef}
          style={[st.preview, st.videoView]}
          type={RNCamera.Constants.Type.front}
          onRecordingStart={onRecordingStart}
          playSoundOnCapture={false}
          ratio={'4:3'}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        >
        </RNCamera>

        <View
          source={{ uri: `data:image/jpeg;base64,${videoData.thumb}` }}
          style={[st.wrap, st.videoView]}
        >
          {videoData.uri && <Video source={{ uri: videoData.uri }}
            ref={videoRef}
            onBuffer={() => console.log('on buffer')}
            onError={(err) => console.log('on error', err)}
            onLoad={onVideoLoad}
            onProgress={onVideoProgress}
            resizeMode="cover"
            style={[st.backgroundVideo, st.videoView]}
          />}
        </View >
      </View>
      <View style={st.btnsWrap}>
        {isRecording && <Btn
          style={st.recordBtn}
          onPress={onStopAndSave}
        >
          Остановить и сохранить
       </Btn>}
        {!isRecording && videoData.uri && !isCountDown &&
          <Btn
            style={st.recordBtn}
            onPress={onStartRecordingPress}
          >
            Запустить запись видео
       </Btn>}
        {!isRecording && !isCountDown &&
          < Btn
            style={st.recordBtn}
            onPress={pickVideo}
          >
            Выбрать видео
          </Btn>
        }
        {isCountDown &&
          <CountDown
            style={{ height: 40 }}
            onEndTime={onEndTime}
          />}
      </View>
    </View >
  );

}

TwoVideos.defaultProps = {
  onStop: () => { },
  onRecordingStart: () => { },
  style: {}
}

const st = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  preview: {
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  wrap: {
    backgroundColor: 'lightgray',
  },
  backgroundVideo: {
    transform: [{ rotate: '0deg' }]
  },
  recordBtn: {
    marginHorizontal: 10
  },
  videoView: {
    width: VIDEO_WIDHT,
    height: VIDEO_HEIGHT,
  },
  videosWrap: {
    height: VIDEO_HEIGHT,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 20
  },
  loader: {
    position: 'absolute',
    zIndex: 3,
  },
  btnsWrap: {
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  }
});

export default TwoVideos
