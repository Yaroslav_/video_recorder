// import React, { useRef, useState } from 'react';
// import { View, StyleSheet, Text } from 'react-native';
// import Video from 'react-native-video';

// const VideoView = (props) => {
//   const [state, setState] = useState({ loading: true, error: false });
//   const videoRef = useRef(null);

//   const onBuffer = () => setState(state => ({ error: false, loading: true }));
//   const videoError = () => setState(state => ({ ...state, error: true }));
//   const onVideoReady = (data) => {
//     props.onVideoReady && props.onVideoReady(data)
//   }

//   if (state.error) {
//     return <View style={[st.wrap, props.style]}>
//       <Text>Sorry, something went wrong.</Text>
//     </View>
//   }

//   return (
//     <View style={[st.wrap, props.style, props.wrapStyle]}>
//       {props.uri &&
//         <Video source={{ uri: props.uri }}      // Can be a URL or a local file.
//           ref={videoRef}                        // Store reference
//           onBuffer={onBuffer}                   // Callback when remote video is buffering
//           onError={videoError}
//           onLoad={onVideoReady}           // Callback when video cannot be loaded
//           style={[st.backgroundVideo, props.style]}
//           {...props.videoProps}
//         />}
//     </View>
//   )
// }

// VideoView.defaultProps = {
//   style: {},
//   uri: "background",
//   videoProps: {},
//   wrapStyle: {}
// }
// // Later on in your styles..
// const st = StyleSheet.create({
//   wrap: {
//     width: 200,
//     height: 200,
//     backgroundColor: 'lightgray'
//   },
//   backgroundVideo: {
//   },
//   loader: {
//     position: 'absolute',
//   }
// });

// export default React.memo(VideoView);