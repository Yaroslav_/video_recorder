import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const InfoCard = props => (
  <View style={[st.wrap, props.style]}>
    <Text style={st.text}>{props.text}</Text>
  </View>
);

InfoCard.defaultProps = {
  text: ''
}

const st = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 16
  }
})

export default InfoCard;