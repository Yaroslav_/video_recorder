import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

const Loader = props => (
  <View style={[st.wrap, props.style]}>
    <ActivityIndicator size="large" color="blue" />
  </View>
);

Loader.defaultProps = {
  style: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
}

const st = StyleSheet.create({
  wrap: {
    height: 40,
    width: 270,
    borderRadius: 8,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center'
  },
})

export default Loader;