import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const CountDown = props => {
  const defTime = props.time || 5;
  const [time, setTime] = useState(defTime);
  const interval = useRef();

  useEffect(() => {
    interval.current = setInterval(() => {
      setTime(time => time - 1);
    }, 1000);
  }, []);

  useEffect(() => {
    if (time === 0) {
      clearInterval(interval.current);
      props.onEndTime && props.onEndTime()
    }
  })

  return (
    <View style={[st.wrap, props.style]}>
      <Text style={st.text}>{time}</Text>
    </View>
  );
}

CountDown.defaultProps = {
  children: 'Press me',
  onPress: () => { }
}

const st = StyleSheet.create({
  wrap: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 20
  }
})

export default CountDown;