import RNFS from 'react-native-fs';

export const getFileData = async (...args) => {
  await RNFS.readFile(...args)
}

export const saveVideo = async (uri) => {
  let fileName = `VID_${Date.now()}.mp4`;
  await RNFS.mkdir(`${RNFS.ExternalDirectoryPath}/MyVideos`)
  await RNFS.copyFile(uri, RNFS.ExternalDirectoryPath + `/MyVideos/${fileName}`);
}