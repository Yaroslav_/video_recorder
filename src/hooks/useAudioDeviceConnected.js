import { useEffect, useState } from 'react';
import HeadphoneDetection from 'react-native-headphone-detection';

const useAudioDeviceConnected = () => {
  const [isConnected, setIsConnected] = useState(false);

  useEffect(() => {
    HeadphoneDetection.addListener((res) => {
      if (res && res.audioJack || res && res.bluetooth) {
        setIsConnected(true);
      } else {
        setIsConnected(false)
      }
    });
    return () => {
      if (HeadphoneDetection.remove) {
        HeadphoneDetection.remove();
      }
    }
  }, [])

  return { isConnected }
};

export default useAudioDeviceConnected;