import 'react-native-gesture-handler';
import React, { useState, useRef, useCallback } from 'react';
import { StyleSheet, StatusBar, View, Dimensions } from 'react-native';
import Btn from '../components/buttons/Btn';
import CountDown from '../components/other/CountDown';
import DefCamera from '../components/camera/DefCamera';
import InfoCard from '../components/cards/InfoCard';
import { saveVideo } from '../helprer-funcs/video';

const { width, height } = Dimensions.get('window');
const VIDEO_HEIGHT = height * 0.6;
const VIDEO_WIDHT = width * 0.4;

const defState = {
  isCountDown: false, isCamera: false,
  showTwoVideos: false, runTwoVideos: false, loading: true
}

const infoCardDefState = {
  error: false, text: '', showCard: false
}

const videosDefState = {
  isCameraReady: false,
}

const Mode1Screen = (props) => {
  const [state, setState] = useState(defState);
  const videoData = useRef();
  const { isCountDown, isCamera } = state;
  const [infoCardState, setInfoCardState] = useState(infoCardDefState);
  const [videosState, setVideosState] = useState(videosDefState)

  const toggleInfoCard = (success) => {
    if (success) {
      setInfoCardState(state => ({ ...state, error: false, showCard: true }))
    } else {
      setInfoCardState(state => ({ ...state, error: true, showCard: true }))
    }
    setTimeout(() => {
      setInfoCardState(infoCardDefState)
    }, 3000)
  }

  const onStopCamera = useCallback(async (uri) => {
    try {
      videoData.current = false;
      if (uri) {
        await saveVideo(uri);
        toggleInfoCard(uri);
      } else {
        toggleInfoCard(false);
      }
      setState(state => ({ ...state, isCamera: false, loading: false }))
      setVideosState(state => ({ ...state, isCameraReady: false }))
      console.log("Video copied locally!!");
    } catch (err) {
      setState(state => ({ ...state, isCamera: false, loading: false }))
      console.log("CopyFile fail for video: " + err);
    }
  }, [])

  const onEndTime = () => {
    setState(state => ({ ...state, isCountDown: false }))
    setState(state => ({ ...state, isCamera: true }))
  }

  const onCameraReadyHandler = useCallback(() => {
    setVideosState(state => ({ ...state, isCameraReady: true }))
  }, [])

  const onStopPress = () => {
    setState(state => ({ ...state, isCamera: false, loading: true }))
  }

  const onRecordPress = () => {
    if (videosState.isCameraReady) {
      setState(state => ({ ...state, isCountDown: true }))
    } else {
      alert('Идет загрузка камеры')
    }
  }

  React.useEffect(() => {
    const focuse = props.navigation.addListener('focus', () => {
      setState(st => ({ ...st, loading: false }))
      setTimeout(() => { setState(st => ({ ...st, loading: false })) }, 200)
    });
    const blur = props.navigation.addListener('blur', () => {
      setState(st => ({ ...st, loading: false }))
      setTimeout(() => { setState(st => ({ ...st, loading: true })) }, 200)
    });

    return () => {
      focuse();
      blur();
    }

  }, []);

  return (


    <View style={st.wrap}>
      <StatusBar barStyle="dark-content" />
      <View style={st.videosWrap}>
        {!infoCardState.showCard && !state.loading &&
          <DefCamera
            style={st.videoView}
            startRecording={videosState.isCameraReady && isCamera ? true : false}
            onCameraReady={onCameraReadyHandler}
            onStop={onStopCamera} style={st.videoView}
          />}
        {infoCardState.showCard && <InfoCard text={infoCardState.error ? "Ошыбка" : "Вы успешно сохранили видео!"} />}
      </View>

      {isCamera && !state.loading ? <Btn
        style={st.recordBtn}
        onPress={onStopPress}>
        Остановить и сохранить
       </Btn> : null}

      {!isCountDown && !isCamera && !infoCardState.showCard && !state.loading &&
        <Btn
          style={st.recordBtn}
          onPress={onRecordPress}>
          Запустить запись видео
       </Btn>}

      {isCountDown &&
        <CountDown
          onEndTime={onEndTime}
        />}
    </View>
  );
};

const st = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center'
  },
  recordBtn: {
  },
  videoView: {
    width: VIDEO_WIDHT,
    height: VIDEO_HEIGHT,
    // padding: 20
  },
  videosWrap: {
    height: VIDEO_HEIGHT,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 20
  }
});

export default Mode1Screen;
