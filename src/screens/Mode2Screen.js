import 'react-native-gesture-handler';
import React, { useState, useCallback } from 'react';
import { StyleSheet, StatusBar, View, Dimensions } from 'react-native';
import InfoCard from '../components/cards/InfoCard';
import TwoVideos from '../components/camera/TwoVideos';
import { saveVideo } from '../helprer-funcs/video';

const { width, height } = Dimensions.get('window');
const VIDEO_HEIGHT = height * 0.6;
const VIDEO_WIDHT = width * 0.4;

const infoCardDefState = {
  error: false, text: '', showCard: false, loading: false
}

const videosDefState = {
  isCameraReady: false, isVideoReady: false, showTwoVideos: false, recording: false
}

const Mode2Screen = () => {
  const [infoCardState, setInfoCardState] = useState(infoCardDefState);
  const [videosState, setVideosState] = useState(videosDefState);

  const toggleInfoCard = (success) => {
    if (success) {
      setInfoCardState(state => ({ ...state, error: false, showCard: true, loading: false }))
    } else {
      setInfoCardState(state => ({ ...state, error: true, showCard: true, loading: false }))
    }
    setTimeout(() => {
      setInfoCardState(infoCardDefState)
    }, 3000)
  }

  const onStopCamera = useCallback(async (uri) => {
    if (!videosState.showTwoVideos) {
      setVideosState(state => ({ ...state, showTwoVideos: false, recording: false }))
    }
    try {
      if (uri) {
        await saveVideo(uri);
        toggleInfoCard(uri);
      } else {
        toggleInfoCard(false);
      }
      if (videosState.showTwoVideos) {
        setVideosState(videosDefState)
      }
      console.log("Video copied locally!!");
    } catch (err) {
      toggleInfoCard(false);
      console.log("CopyFile fail for video: " + err);
    }
  }, [])

  return (
    <View style={st.wrap}>
      <StatusBar barStyle="dark-content" />

      {!infoCardState.showCard && !infoCardState.loading &&
        <TwoVideos
          onStop={onStopCamera}
          videoStyle={st.videoView}
        />
      }
      {infoCardState.showCard && <InfoCard text={infoCardState.error ? "Ошыбка" : "Вы успешно сохранили видео!"} />}

    </View>
  );
};

const st = StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
  },
  recordBtn: {
    marginHorizontal: 10
  },
  videoView: {
    width: VIDEO_WIDHT,
    height: VIDEO_HEIGHT,
  },
  videosWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 20
  },
  loader: {
    position: 'absolute',
    zIndex: 3,
  }
});

export default Mode2Screen;
